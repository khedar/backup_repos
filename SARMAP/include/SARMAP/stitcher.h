#pragma once

#include <memory>
#include <iostream>
#include <string>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

class Stitcher
{
public:
    struct Measurement
    {
        cv::Mat img;
        cv::Mat pose;
    };
    typedef std::shared_ptr<Stitcher::Measurement> MeasurementPtr;
    
    struct Plane
    {
        cv::Point3d root;
        cv::Vec3d ez;
    };
    typedef std::shared_ptr<Stitcher::Plane> PlanePtr;
    
public:
    Stitcher(const std::string& pDir, const cv::Mat& pK, const cv::Mat& pDistCoeffs);
    bool Process(const cv::Mat& pImg, const cv::Mat& pPose, cv::Mat& rImg);
    void SaveMap();
    
private:
    bool mInitialized;
    
    std::string mSaveDir;
    
    uint16_t mNrofPrcssdMmts;
    
    cv::Mat mK;
    cv::Mat mDistCoeffs;
    
    cv::Point mvRoot;
    cv::Mat mvMap;
    
    PlanePtr mRefplane;

    cv::Point3d mPrevPos;
    
    cv::Point2i ApplyHomographyToPoint(const int &pX, const int &pY, const cv::Mat &pH);
    
    cv::Mat ComputeImageBounds2D(const MeasurementPtr& pMmt);
    cv::Mat ComputeImageBoundsInMap(const MeasurementPtr& pMmt, double pDepth = -1.0);
    cv::Mat ComputeImageBoundsInPlane(const MeasurementPtr& pMmt);
    
    void MapImageToPlane(const cv::Mat &pImg, const cv::Mat pH);
};
typedef std::shared_ptr<Stitcher> StitcherPtr;
