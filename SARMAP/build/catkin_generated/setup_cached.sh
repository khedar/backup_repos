#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/lachs/Documents/Workspace/catkin_ws/Telekom/src/SARMAP/build/devel:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/lachs/Documents/Workspace/catkin_ws/Telekom/src/SARMAP/build/devel/lib:$LD_LIBRARY_PATH"
export PKG_CONFIG_PATH="/home/lachs/Documents/Workspace/catkin_ws/Telekom/src/SARMAP/build/devel/lib/pkgconfig:$PKG_CONFIG_PATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/lachs/Documents/Workspace/catkin_ws/Telekom/src/SARMAP/build/devel/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/lachs/Documents/Workspace/catkin_ws/Telekom/src/SARMAP:$ROS_PACKAGE_PATH"