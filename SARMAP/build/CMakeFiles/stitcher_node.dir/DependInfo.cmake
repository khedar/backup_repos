# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/lachs/Documents/Workspace/catkin_ws/Telekom/src/SARMAP/src/stitcher_node.cpp" "/home/lachs/Documents/Workspace/catkin_ws/Telekom/src/SARMAP/build/CMakeFiles/stitcher_node.dir/src/stitcher_node.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_PACKAGE_NAME=\"SARMAP\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "/home/lachs/Documents/Workspace/catkin_ws/Telekom/devel/include"
  "/opt/ros/kinetic/include"
  "/opt/ros/kinetic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  "/opt/ros/kinetic/include/opencv-3.2.0-dev"
  "/opt/ros/kinetic/include/opencv-3.2.0-dev/opencv"
  "/usr/include/eigen3"
  "/usr/local/include/exiv2"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/lachs/Documents/Workspace/catkin_ws/Telekom/src/SARMAP/build/CMakeFiles/SARMAP.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
