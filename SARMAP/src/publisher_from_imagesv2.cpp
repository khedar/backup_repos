// This node was written for using with A65 Images from MicroDrones for the Telekom Project.
// Publishes only Geo_image_compressed after reading the ExifTagged thermal Images

#include <iostream>
#include <ros/ros.h>
#include <rosbag/bag.h>
#include <boost/filesystem.hpp>

#include <cv_bridge/cv_bridge.h>


#include <sensor_msgs/Image.h>
#include <sensor_msgs/NavSatFix.h>
#include "custom_msgs/GeoImageCompressed.h"


#include <std_msgs/Time.h>
#include <std_msgs/Header.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <sensor_msgs/image_encodings.h>

#include <exiv2/exiv2.hpp>


namespace fs = boost::filesystem;
// TODO:: Constant Heading and Altitude from launch file.

std::vector<std::string> 	get_file_list(const std::string& path);
void 						PublishGeoImage(std::string& aImagePath, ros::Publisher& aPublisher, float baroHeight);


int main(int argc, char **argv)
{
    ros::init(argc, argv, "publisher_from_images_node");
	ros::NodeHandle lnh,l_pv_nh("~");

    std::string l_image_path,l_image_topic,l_geo_image_topic;
    float l_freq, l_altitude;

    l_pv_nh.param("image_path", l_image_path, std::string("uninitialised"));
    l_pv_nh.param("image_topic", l_image_topic, std::string("uninitialised"));
    l_pv_nh.param("geoimage_topic", l_geo_image_topic, std::string("uninitialised"));
    l_pv_nh.param("fps", l_freq, float(0));
    l_pv_nh.param("altitude", l_altitude, float(-1000));


    // Read the files into the list.
    std::vector<std::string> l_image_list = get_file_list(l_image_path);
    std::vector<std::string> l_current_img_list, l_new_imgages_list;
    std::sort(l_image_list.begin(),l_image_list.end());


    ROS_INFO("File input Image Path is: %s", l_image_path.c_str());
    ROS_INFO("Image publish topic is: %s", l_image_topic.c_str());
    ROS_INFO("GeoImage publish topic is: %s", l_geo_image_topic.c_str());
    ROS_INFO("Image publish frequency is: %f", l_freq);
    ROS_INFO("Relative Altitude for the mission is: %f", l_altitude);
    std::cout << "No of Input images are: " << l_image_list.size() << std::endl;

    ros::Publisher image_pub = lnh.advertise<sensor_msgs::Image>(l_image_topic, 2);
    ros::Publisher geo_image_pub = lnh.advertise<custom_msgs::GeoImageCompressed>(l_geo_image_topic, 2);
    
    ros::Rate loop_rate(l_freq);
    
    int count = 0;
    int rosspincounter = 0;
    while (ros::ok() )
    {
    	//- Process the image list.
    	if ( count < l_image_list.size() )
    	{
    		ROS_INFO_STREAM("Image No: " << count << "Image Path: " << l_image_list[count]);
        	PublishGeoImage(l_image_list[count], geo_image_pub,l_altitude);
        	count++;
        	ROS_INFO_STREAM("Count is: " << count);
    	}

    	//- If all images processed, check for new images and add to the l_image_list
    	if ( count == l_image_list.size() && (rosspincounter % 10 == 0 ) )
    	{
    		ROS_INFO_STREAM("All old images processed. Checking for new images now.");
    		l_current_img_list.clear();
    		l_new_imgages_list.clear();
    		l_current_img_list = get_file_list(l_image_path);
    		ROS_INFO_STREAM("Image List size old and new are. " << l_image_list.size() << " : " \
    			<< l_current_img_list.size() );
    		if (l_image_list.size() != l_current_img_list.size())
    		{
    			std::vector<std::string> tmpHolder(l_image_list);
    			std::sort(tmpHolder.begin(),tmpHolder.end());
    			std::sort(l_current_img_list.begin(),l_current_img_list.end());
    			std::set_difference(l_current_img_list.begin(), l_current_img_list.end(), \
    				tmpHolder.begin(), tmpHolder.end(), std::back_inserter(l_new_imgages_list));
    			ROS_INFO_STREAM("No. of new images: " << l_new_imgages_list.size() );
    			std::sort(l_new_imgages_list.begin(),l_new_imgages_list.end());
    			l_image_list.insert(l_image_list.end(), l_new_imgages_list.begin(), l_new_imgages_list.end());
    			tmpHolder.clear();
    		}
    		ROS_INFO("No new images. Continuing processing.");
    	}
    	ros::spinOnce();
    	rosspincounter++;
    	loop_rate.sleep();
    }
    return 0;
}


void PublishGeoImage(std::string& aImagePath, ros::Publisher& aPublisher, float baroHeight)
{

    if (aImagePath.empty()) 
    {
        ROS_INFO("No Image found.");
        return;
    }
    Exiv2::Image::AutoPtr image = Exiv2::ImageFactory::open(aImagePath);
    sensor_msgs::NavSatFix gpsdata;
    float image_heading;

    if (image.get() != 0)
    {
        image->readMetadata();
        Exiv2::ExifData &exifData = image->exifData();
        if (exifData.empty()) 
        {
            std::string error(aImagePath);
            error += ": No Exif data found in the file";
            std::cout << error << std::endl;
        }

        gpsdata.latitude  = exifData["Exif.GPSInfo.GPSLatitude"].toFloat();            
        gpsdata.longitude = exifData["Exif.GPSInfo.GPSLongitude"].toFloat();            
        gpsdata.altitude  = exifData["Exif.GPSInfo.GPSAltitude"].toFloat();
        image_heading  = exifData["Exif.GPSInfo.GPSImgDirection"].toFloat();
        std::cout.precision(7);
        std::cout <<  " Image is: " << aImagePath << std::endl;
        std::cout << "\n        Lat : Long: Alt: RelAlt: Heading" << std::endl;
        std::cout << "        " << gpsdata.latitude << " : " << gpsdata.longitude << " : " \
              << gpsdata.altitude << " : " << baroHeight << " : " << image_heading << "\n" << std::endl;


    }
    custom_msgs::GeoImageCompressed geoCompressedImagemsg_;

    cv::Mat im = cv::imread(aImagePath,CV_LOAD_IMAGE_COLOR);
    cv_bridge::CvImage cvImage;
    cvImage.image = im;
    cvImage.encoding = sensor_msgs::image_encodings::RGB8;

    geoCompressedImagemsg_.imagedata.format = "bgr8";
    //geoCompressedImagemsg_.imagename.data=image_name_.substr(save_location_.length());
    geoCompressedImagemsg_.imagedata = *cvImage.toCompressedImageMsg();
    geoCompressedImagemsg_.gpsdata=gpsdata;
    geoCompressedImagemsg_.heading.data = image_heading;
    geoCompressedImagemsg_.baroHeight.data = baroHeight;
    geoCompressedImagemsg_.header.stamp = ros::Time::now();
    //geoCompressedImagemsg_.missioncount.data = mMissionCount;
    aPublisher.publish(geoCompressedImagemsg_);

    
}

std::vector<std::string> get_file_list(const std::string& path)
{
    std::vector<std::string> m_file_list;
    if (!path.empty())
    {
        fs::path apk_path(path);
        fs::recursive_directory_iterator end;

        for (fs::recursive_directory_iterator i(apk_path); i != end; ++i)
        {
            const fs::path cp = (*i);
            m_file_list.push_back(cp.string());
        }
    }
    return m_file_list;
}

