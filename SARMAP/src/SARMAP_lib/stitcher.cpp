#include "SARMAP/stitcher.h"

Stitcher::Stitcher(const std::string& pDir, const cv::Mat& pK, const cv::Mat& pDistCoeffs)
{
    mInitialized = false;
    
    mSaveDir = pDir;
    
    mNrofPrcssdMmts = 0;
    
    mK = pK;
    mDistCoeffs = pDistCoeffs;
    
    cv::Point3d root(0.0, 0.0, 0.0);
    cv::Vec3d ez(0.0, 0.0, 1.0);
    mRefplane = std::make_shared<Plane>();
    mRefplane->root = root;
    mRefplane->ez = ez;
}

bool Stitcher::Process(const cv::Mat& pImg, const cv::Mat& pPose, cv::Mat& rImg)
{
    mNrofPrcssdMmts++;

    //if (mNrofPrcssdMmts % 10 != 0 && mNrofPrcssdMmts != 1)
    //{
    //    return 0;
    //}
    
    auto mmt = std::make_shared<Measurement>();
    mmt->img = pImg;
    mmt->pose = pPose;

    if (!mInitialized)
    {
        int width = mmt->img.cols;
        int height = mmt->img.rows;
        
        mRefplane->root.x = pPose.at<double>(0, 3);
        mRefplane->root.y = pPose.at<double>(1, 3);
        mRefplane->root.z = pPose.at<double>(2, 3);
        
        mvMap = cv::Mat(width, height, CV_8UC4, cv::Scalar(0, 0, 0, 0));
        mvRoot = cv::Point2i(width/2, height/2);
        
        cv::namedWindow("Map", CV_WINDOW_NORMAL);
        
        mInitialized = true;
    }
    else
    {
        cv::Point3d pos;
        pos.x = pPose.at<double>(0, 3);
        pos.y = pPose.at<double>(1, 3);
        pos.z = pPose.at<double>(2, 3);

        cv::Point3d dpos = pos - mPrevPos;

        std::cout << "Angle: " << atan2(dpos.y, dpos.x)*180 / M_PI << std::endl;
    }
    
    mmt->pose.at<double>(0, 3) = mmt->pose.at<double>(0, 3) - mRefplane->root.x;
    mmt->pose.at<double>(1, 3) = mRefplane->root.y - mmt->pose.at<double>(1, 3);
    
    //std::cout << "MMT Pose:\n" << mmt->pose << std::endl;
    
    cv::Mat boundsUndistorted = ComputeImageBounds2D(mmt);
    cv::Mat boundsInPlane = ComputeImageBoundsInPlane(mmt);
    
    //std::cout << "bounds:\n" << boundsInPlane << std::endl;
    
    cv::Point2d centroid(cv::mean(boundsInPlane.col(0))[0], cv::mean(boundsInPlane.col(1))[0]);
    std::cout << "centroid: " << centroid << std::endl;
	    
    std::vector<cv::Point2f> boundPointsImg;
    std::vector<cv::Point2f> boundPointsPlane;
    
    for (size_t i = 0; i < boundsUndistorted.rows; i++)
    {
	    cv::Point2f ptImg((float)boundsUndistorted.at<double>(i, 0), (float)boundsUndistorted.at<double>(i, 1));
	    cv::Point2f ptPlane(10.0*(float)boundsInPlane.at<double>(i, 0), 10.0*(float)boundsInPlane.at<double>(i, 1));
	    boundPointsImg.push_back(ptImg);
	    boundPointsPlane.push_back(ptPlane);
    }
    
    cv::Mat H = cv::getPerspectiveTransform(boundPointsPlane, boundPointsImg);

    std::cout << "Measurement ID: " << mNrofPrcssdMmts << std::endl;
    
    cv::Mat imgUndistorted;
    cv::undistort(mmt->img, imgUndistorted, mK, mDistCoeffs);
    MapImageToPlane(imgUndistorted, H);

    mPrevPos.x = pPose.at<double>(0, 3);
    mPrevPos.y = pPose.at<double>(1, 3);
    mPrevPos.z = pPose.at<double>(2, 3);
    
    cv::imshow("Map", mvMap);
    cv::waitKey(100);
    
    rImg = mvMap;
    
    return 1;    
}


void Stitcher::SaveMap()
{
    cv::imwrite(mSaveDir + std::string("/map.png"), mvMap);
}


inline cv::Point2i Stitcher::ApplyHomographyToPoint(const int &pX, 
                                                      const int &pY, 
                                                      const cv::Mat &pH) {
    cv::Point2i point;
    double z = (pH.at<double>(2, 0) * pX + pH.at<double>(2, 1) * pY + pH.at<double>(2, 2));
    double scale = 1./z;
    point.x = cvRound((pH.at<double>(0, 0) * pX + pH.at<double>(0, 1) * pY + pH.at<double>(0, 2)) * scale);
    point.y = cvRound((pH.at<double>(1, 0) * pX + pH.at<double>(1, 1) * pY + pH.at<double>(1, 2)) * scale);
    return point;
}


cv::Mat Stitcher::ComputeImageBounds2D(const MeasurementPtr &pMmt) 
{    
    cv::Mat imgBounds = (cv::Mat_<double>(4, 2, CV_64F) << 0, 0, 
                                                           0, (double) pMmt->img.rows,
                                                           (double) pMmt->img.cols,(double) pMmt->img.rows,
                                                           (double) pMmt->img.cols, 0);
        
    imgBounds = imgBounds.reshape(2);
    cv::undistortPoints(imgBounds, imgBounds, mK, mDistCoeffs, cv::Mat(), mK);
    imgBounds = imgBounds.reshape(1);

    return imgBounds;
}  

cv::Mat Stitcher::ComputeImageBoundsInPlane(const MeasurementPtr &pMmt)
{
    cv::Mat p = (cv::Mat_<double>(3, 1, CV_64F) << 0, 0, 0); // TODO: Hmm, doof eig, ausbessern
    cv::Mat n = (cv::Mat_<double>(3, 1, CV_64F) << mRefplane->ez(0), mRefplane->ez(1), mRefplane->ez(2));
    cv::Mat R = pMmt->pose.rowRange(0,3).colRange(0,3);
    cv::Mat t = pMmt->pose.rowRange(0,3).col(3);
 
    cv::Mat imgBounds = ComputeImageBounds2D(pMmt); 
    
    cv::Mat planePoints;
    for (uint i = 0; i < 4; i++) 
    {
        cv::Mat u = (cv::Mat_<double>(3, 1, CV_64F) << imgBounds.at<double>(i, 0), imgBounds.at<double>(i, 1), 1.0);
        double s = (p - t).dot(n) / (R*mK.inv()*u).dot(n);
        cv::Mat pt = s*(R*mK.inv()*u) + t;
        cv::Mat mapPoint = (cv::Mat_<double>(1, 3, CV_64F) << pt.at<double>(0), pt.at<double>(1), pt.at<double>(2));
        planePoints.push_back(mapPoint);
    }

    return planePoints;
}

cv::Mat Stitcher::ComputeImageBoundsInMap(const MeasurementPtr &pMmt, double pDepth)
{
    cv::Mat R = pMmt->pose.colRange(0,3).rowRange(0,3);
    cv::Mat t = pMmt->pose.col(3).rowRange(0,3);

    cv::Mat imgBounds = ComputeImageBounds2D(pMmt); 
    
    cv::Mat boundsInMap;
    for (uint i = 0; i < 4; i++) 
    {
        cv::Mat x = (cv::Mat_<double>(3, 1, CV_64F) << imgBounds.at<double>(i, 0), imgBounds.at<double>(i, 1), 1.0);
        cv::Mat pt = R*(mK.inv()*pDepth*x) + t;
        cv::Mat mapPoint = (cv::Mat_<double>(1, 3, CV_64F) << pt.at<double>(0), pt.at<double>(1), pt.at<double>(2));
        boundsInMap.push_back(mapPoint);
    }
    return boundsInMap;
}

void Stitcher::MapImageToPlane(const cv::Mat &pImg, const cv::Mat pH)
{
    cv::Mat Hinv = pH.inv();

    int width = pImg.cols;
    int height = pImg.rows;

    bool changedDim = false;
    double xMinus = 0;
    double xPlus = 0;
    double yMinus = 0;
    double yPlus = 0;

    cv::Point2i deformedULC = ApplyHomographyToPoint(0, 0, Hinv);
    cv::Point2i deformedLLC = ApplyHomographyToPoint(0, height, Hinv);
    cv::Point2i deformedURC = ApplyHomographyToPoint(width, 0, Hinv);
    cv::Point2i deformedLRC = ApplyHomographyToPoint(width, height, Hinv);
		
    cv::Point2i roiULC;
    roiULC.x = std::min({deformedULC.x, deformedLLC.x, deformedLRC.x, deformedURC.x});
    roiULC.y = std::min({deformedULC.y, deformedLLC.y, deformedLRC.y, deformedURC.y});
    
    cv::Point2i roiLRC;
    roiLRC.x = std::max({deformedULC.x, deformedLLC.x, deformedLRC.x, deformedURC.x});
    roiLRC.y = std::max({deformedULC.y, deformedLLC.y, deformedLRC.y, deformedURC.y});

    if (mvRoot.x + roiULC.x < 0 + width/2) 
    {
        xMinus = abs(width/2 - (roiULC.x + mvRoot.x));
        mvRoot.x += xMinus;
        changedDim = true;
    }
    
    if (mvRoot.y + roiULC.y < 0 + height/2) 
    {
        yMinus = abs(height/2 - (roiULC.y + mvRoot.y));
        mvRoot.y += yMinus;
        changedDim = true;
    }

    if (mvRoot.x + roiLRC.x > mvMap.cols - width/2)
    {
        xPlus = (roiLRC.x + mvRoot.x) - (mvMap.cols - width/2);
        changedDim = true;
    }
    
    if (mvRoot.y + roiLRC.y > mvMap.rows - height/2) 
    {
        yPlus = (roiLRC.y + mvRoot.y) - (mvMap.rows - height/2);
        changedDim = true;
    }
    
    if (changedDim == true)
    	cv::copyMakeBorder(mvMap, mvMap, yMinus, yPlus, xMinus, xPlus, cv::BORDER_CONSTANT, 0);
	      
    for (int r = roiULC.y + mvRoot.y; r < mvRoot.y + roiLRC.y; r++) 
    {
        for (int c = roiULC.x + mvRoot.x; c < roiLRC.x + mvRoot.x; c++) 
        {
            cv::Point2i transformed = ApplyHomographyToPoint(c - mvRoot.x, r - mvRoot.y, pH);
            if (transformed.x > 0 && transformed.x < width && transformed.y > 0 && transformed.y < height) 
            {
		cv::Vec4b pixel = pImg.at<cv::Vec4b>(transformed.y, transformed.x);
		if (pixel[0] == 255 && pixel[1] == 0 && pixel[2] == 0)
		  continue;
                mvMap.at<cv::Vec4b>(r, c) = pixel; 
            }
        }
    }
}
