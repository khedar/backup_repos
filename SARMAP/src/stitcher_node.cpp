#include <memory.h>

#include <ros/ros.h>
#include <geodesy/utm.h>
#include <geodesy/wgs84.h>
#include <tf/tf.h>
#include <cv_bridge/cv_bridge.h>

#include <opencv2/core/core.hpp>

#include <geographic_msgs/GeoPoint.h>
#include <custom_msgs/GeoImageCompressed.h>
#include <sensor_msgs/Image.h>

#include <ros/package.h>

#include <SARMAP/stitcher.h>

namespace SARMAP
{
 
  
class StitcherNode
{
public:
    StitcherNode(const std::string& topicGeoimg, 
		 const std::string& topicStitchedimg, 
		 const double& mounting, 
		 const double& resizing,
		 const cv::Mat& K, 
		 const cv::Mat& distCoeffs);
    int Spin();
    int IsOkay();
    void SaveMap();
    
private:

    double mMounting;

    double mResizing;
    
    cv::Mat mK;
    cv::Mat mDistCoeffs;

    StitcherPtr mStitcher;
  
    ros::NodeHandle mNodeHandle;

    ros::Subscriber mGeoimgSub;
    
    ros::Publisher mStitchimgPub;
    
    void SubGeoimg(const custom_msgs::GeoImageCompressed& pGeoimg);
    
    void PubStitched(const cv::Mat& pImg);
};


}

using namespace SARMAP;

int main(int argc, char** argv) 
{
    ros::init(argc, argv, "SARMAP_stitcher");
    
    std::string geoimage_topic, stitched_image_topic;
    double mounting;
    double resizing;
    double fps;
    double fx, fy;
    double cx, cy;
    double k1, k2, p1, p2, p3, k3;
    
    ros::NodeHandle nh("~");
    nh.param("topic_geoimg", geoimage_topic, std::string("/geo_image_manta"));
    nh.param("topic_stitchedimg", stitched_image_topic, std::string("/SARMAP/stitched_image"));
    nh.param("camera/mounting", mounting, -1.0);
    nh.param("camera/fps", fps, 0.0);
    nh.param("camera/fx", fx, 0.0);
    nh.param("camera/fy", fy, 0.0);
    nh.param("camera/cx", cx, 0.0);
    nh.param("camera/cy", cy, 0.0);
    nh.param("camera/k1", k1, 0.0);
    nh.param("camera/k2", k2, 0.0);
    nh.param("camera/p1", p1, 0.0);
    nh.param("camera/p2", p2, 0.0);
    nh.param("camera/k3", k3, 0.0);
    nh.param("stitcher/resizing", resizing, 1.0);
    
    cv::Mat K = cv::Mat::eye(3, 3, CV_64F);
    K.at<double>(0, 0) = resizing * fx;
    K.at<double>(1, 1) = resizing * fy;
    K.at<double>(0, 2) = resizing * cx;
    K.at<double>(1, 2) = resizing * cy;
    
    cv::Mat distCoeffs = cv::Mat::zeros(1, 5, CV_64F);
    distCoeffs.at<double>(0) = k1;
    distCoeffs.at<double>(1) = k2;
    distCoeffs.at<double>(2) = p1;
    distCoeffs.at<double>(3) = p2;
    distCoeffs.at<double>(4) = k3;
    
    StitcherNode stitcher(geoimage_topic, stitched_image_topic, mounting, resizing, K, distCoeffs);
	
    ros::Rate rate(fps);
    
    while(stitcher.IsOkay()) 
    {
        stitcher.Spin();
        ros::spinOnce();
        rate.sleep();
    }
    
    stitcher.SaveMap();
	
    ros::shutdown();
	
    return 0;
}


StitcherNode::StitcherNode(const std::string& topicGeoimg, 
			   const std::string& topicStitchedimg, 
			   const double& mounting,
			   const double& resizing,
			   const cv::Mat& K, 
			   const cv::Mat& distCoeffs)
: mMounting(mounting), mResizing(resizing), mK(K), mDistCoeffs(distCoeffs)
{ 
    mStitcher = std::make_shared<Stitcher>(ros::package::getPath("SARMAP"), mK, mDistCoeffs);

    mGeoimgSub = mNodeHandle.subscribe(topicGeoimg, 5, &StitcherNode::SubGeoimg, this, ros::TransportHints());
    
    mStitchimgPub = mNodeHandle.advertise<sensor_msgs::Image>(topicStitchedimg, 200);
    
    std::cout << "Started Stitcher with parameters\n- Calibration matrix:\n" << mK << "\nDistortion coeffs:\n" << mDistCoeffs << std::endl;
    std::cout << "Mounting: " << mMounting << "\nResizing: " << resizing << std::endl;
}


int StitcherNode::Spin()
{	
    
}


int StitcherNode::IsOkay()
{
    if (mNodeHandle.ok())
      return 1;
    else
      return 0;
}


void StitcherNode::SubGeoimg(const custom_msgs::GeoImageCompressed& pGeoimg)
{
    cv_bridge::CvImagePtr imgPtr = cv_bridge::toCvCopy(pGeoimg.imagedata, "bgra8");
    cv::Mat img = imgPtr->image;
    cv::resize(img, img, cv::Size(), mResizing, mResizing);
    
    geographic_msgs::GeoPoint wgs;
    wgs.latitude = pGeoimg.gpsdata.latitude;
    wgs.longitude = pGeoimg.gpsdata.longitude;
    wgs.altitude = pGeoimg.baroHeight.data;
    geodesy::UTMPoint utm(wgs);
    
    cv::Mat t = cv::Mat::zeros(3, 1, CV_64F);
    t.at<double>(0) = utm.easting;
    t.at<double>(1) = utm.northing;
    t.at<double>(2) = utm.altitude;
    
    ROS_INFO("Received geoimg with [north: %8.2f, east: %8.2f, alt: %4.2f, heading: %4.2f]", utm.northing, utm.easting, utm.altitude, pGeoimg.heading.data);
    
    cv::Mat rotX = cv::Mat::eye(3, 3, CV_64F);
    //rotX.at<double>(1, 1) = 1;
    rotX.at<double>(2, 2) = mMounting;
    
    double gamma = ((double)pGeoimg.heading.data) * M_PI / 180;
    //double gamma = 84 * M_PI / 180;;
    cv::Mat rotZ = cv::Mat::eye(3, 3, CV_64F);
    rotZ.at<double>(0, 0) = cos(gamma);
    rotZ.at<double>(0, 1) = -sin(gamma);
    rotZ.at<double>(1, 0) = sin(gamma);
    rotZ.at<double>(1, 1) = cos(gamma);   
    
    cv::Mat R = rotZ * rotX;
    
    cv::Mat pose = cv::Mat::eye(3, 4, CV_64F);
    R.col(0).copyTo(pose.col(0));
    R.col(1).copyTo(pose.col(1));
    R.col(2).copyTo(pose.col(2));
    t.col(0).copyTo(pose.col(3));
    
    //std::cout << "Pose:\n" << pose << std::endl;
    
    cv::Mat stitchedImg;
    double t1 = ros::Time::now().toSec();
    if (mStitcher->Process(img, pose, stitchedImg))
    {
        double t2 = ros::Time::now().toSec();
        std::cout << "heading: " << pGeoimg.heading.data << std::endl;
        std::cout << "Time processing frame: " << t2-t1 << "s." << std::endl;
	PubStitched(stitchedImg);
        mStitcher->SaveMap();
    }
   
}


void StitcherNode::PubStitched(const cv::Mat& pImg)
{
    std_msgs::Header header;
    header.stamp = ros::Time::now();
    
    sensor_msgs::Image msg;
    msg = *cv_bridge::CvImage(header, "bgra8", pImg).toImageMsg();
    
    ROS_WARN("Publishing stitched image of size: [%i x %i]", pImg.cols, pImg.rows);
    
    mStitchimgPub.publish(msg);
}


void StitcherNode::SaveMap()
{
    mStitcher->SaveMap();
}
