import rospy
from sensor_msgs.msg import Image
from std_msgs.msg import String
from cv_bridge import CvBridge
import cv2
import numpy as np
import tensorflow as tf
import os
import slim.nets.cifarnet as cifarnet
import slim as slim


class RosTensorFlow():
    def __init__(self):
        self._session = tf.Session()
        self._cv_bridge = CvBridge()
        self._sub_classify = rospy.Subscriber('/camera/image', Image, self.classifyCb, queue_size=10)
        self._sub_live_train = rospy.Subscriber('/camera/image', Image, self.liveTrainCb, queue_size=10)
        self._pub = rospy.Publisher('/result', String, queue_size=10)
   
    def liveTrainCb(self, image_msg):
    	print("Live Training currently not implemented.")
    def classifyCb(self, image_msg):
        cv_image = self._cv_bridge.imgmsg_to_cv2(image_msg, "bgr8")
        image_data = cv2.imencode('.jpg', cv_image)[1].tostring()
        cv2.imshow("Image window", cv_image)
        cv2.waitKey(3)
        #print("Callback function")

        with tf.Graph().as_default() as g:
        	
        	image = tf.image.decode_jpeg(image_data,channels=3)
        	image = tf.image.resize_images(image, [32, 32])
        	float_image = tf.cast(image, tf.float32)
        	images = tf.expand_dims(float_image,0)

    		logits, _ = cifarnet.cifarnet(images, num_classes=3)
    		probabilities = tf.nn.softmax(logits)
    		predictions = tf.argmax(logits, 1)

    		variable_averages = tf.train.ExponentialMovingAverage(0.9999)
    		variables_to_restore = variable_averages.variables_to_restore()

    		saver = tf.train.Saver(variables_to_restore)
    		summary_op = tf.summary.merge_all()
    		summary_writer = tf.summary.FileWriter('/home/ladmin/akamav_ws/src/image_classifier/rostensorflow/summary/cifarnet',g)
    		with tf.Session() as sess:
	    		ckpt = tf.train.get_checkpoint_state("/home/ladmin/akamav_ws/src/image_classifier/rostensorflow/ckpt/cifarnet")
	    		if ckpt and ckpt.model_checkpoint_path:
	    			saver.restore(sess, ckpt.model_checkpoint_path)
	    			global_step = ckpt.model_checkpoint_path.split('/')[-1].split('-')[-1]
	    		else:
	    			print('No checkpoint file found.')
	    			return
	    		logits, probabilities, predictions = sess.run([logits, probabilities, predictions])
	    		print(logits)
	    		print(probabilities)
	    		print(predictions)


    def main(self):
        rospy.spin()

if __name__ == '__main__':
    rospy.init_node('cifarnet_classifier')
    tensor = RosTensorFlow()
    tensor.main()