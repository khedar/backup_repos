import rospy
from sensor_msgs.msg import Image
from std_msgs.msg import String
from cv_bridge import CvBridge
import cv2
import numpy as np
import tensorflow as tf
import os
import slim.nets.cifarnet as cifarnet
import slim as slim

graph_path   = "/home/ladmin/akamav_ws/src/image_classifier/cnn/out.pb"
image_topic  = "/camera/image"
#image_topic  = "/image_raw"

result_topic = "/result"
labelsFilePath   = "/home/ladmin/akamav_ws/src/image_classifier/cnn/output_labels.txt"

class RosTensorFlow():
    def __init__(self):
        self._session = tf.Session()
        self.createGraph()
        self._cv_bridge = CvBridge()
        self._sub_classify = rospy.Subscriber(image_topic, Image, self.classifyCb, queue_size=10)
        self._pub = rospy.Publisher(result_topic, String, queue_size=10)
   
    def createGraph(self):
    	with tf.gfile.FastGFile(graph_path) as file:
    		graph_def = tf.GraphDef()
    		bytess = graph_def.ParseFromString(file.read())
    		print("Graph was read successfully.")
    		print(bytess)
    		#print('Hi')
    		_ = tf.import_graph_def(graph_def,name='')

    def classifyCb(self, image_msg):
        cv_image = self._cv_bridge.imgmsg_to_cv2(image_msg, "bgr8")
        resized_image = cv2.resize(cv_image,(299,299), interpolation=cv2.INTER_LINEAR)

        #image_data = cv2.imencode('.jpg', cv_image)[1].tostring()
        image_data = cv2.imencode('.jpg', resized_image)[1].tostring()

        cv2.imshow("Image window", resized_image)
        cv2.waitKey(3)
        #print("Callback function")

        softmax_tensor = self._session.graph.get_tensor_by_name('final_result:0')
        predictions = self._session.run(softmax_tensor, {'DecodeJpeg/contents:0':image_data})
        predictions = np.squeeze(predictions)

        #print(predictions)
        self.printClassName(predictions)

    def printClassName(self,predictions):
    	labelsFile = open(labelsFilePath,'rb')
    	lines = labelsFile.readlines()
    	labels = [str(w).replace("\n","") for w in lines]
    	print('Probability of Matrix in Image is: %.5f' % (predictions[0]))
    	# for node_id in range(0,len(labels)):
    	# 	print('Probability of Matrix in Image is: %s (score = %.5f)' % (labels[node_id],predictions[node_id]))


    def main(self):
        rospy.spin()

if __name__ == '__main__':
    rospy.init_node('inception_classifier')
    tensor = RosTensorFlow()
    tensor.main()