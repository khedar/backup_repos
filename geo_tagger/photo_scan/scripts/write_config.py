#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 27 10:44:47 2016

@author: Tscherning
"""
import configparser,sys,os
import exiftool
import operator,csv

def createPSImageList(image_path):
    list_file = open(image_path+'/list.csv','w')
    file_list_sorted=os.listdir(image_path)
    file_list_sorted.sort()
    with exiftool.ExifTool() as et:
        for file in file_list_sorted:
            if file.endswith(".jpg"):
                file_path = os.path.join(image_path, file)
                imgNumber = et.get_tag("ImageNumber", file_path)
                dateTimeString = et.get_tag("DateTimeOriginal", file_path)
                tmp_lst=dateTimeString.split(' ')[1].split(':')
                seconds=float(tmp_lst[0])*3600+float(tmp_lst[1])*60+float(tmp_lst[2])
                list_file.write(file_path+','+str(seconds)+'\n')
    list_file.close()

    data = csv.reader(open(image_path+'/list.csv'),delimiter=',')
    sortedlist = sorted(data,key=operator.itemgetter(1))

    initTime=float(sortedlist[0][1])
    final_list_file = open(image_path+'/list.txt','w')
    final_list_file.write(sortedlist[0][0]+'\n')
    for element in sortedlist:
        second=float(element[1])
        if second - initTime > 3:
            final_list_file.write(element[0]+'\n')
            initTime = second
    final_list_file.close()

def main():
    '''Displays usefull information'''
    '''saves choosed options in config.ini'''
    config = configparser.ConfigParser()

    cwd = os.path.dirname(os.path.abspath(__file__))
    config_file_path = cwd+'/config_cor.ini'

    config.read(config_file_path)
    config.set("PATHS","input_images_path",sys.argv[1])
    with open(config_file_path,'w') as cfgfile:
    	config.write(cfgfile)
    createPSImageList(sys.argv[1])

if __name__ == "__main__":
    main()
