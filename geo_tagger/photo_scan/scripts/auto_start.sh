#!/bin/bash

# Autostart für PhotoScan und automatisches Laden eines PS-Python3-Skriptes
# Getestet auf Ubuntu 16.04LTS

echo "auto_start.sh gestartet!"

setxkbmap de



ps_start_path=$1
ps_automation_path=$2

bash $ps_start_path &
sleep 2 




WID=$(xdotool search Agisoft | head -n1)


xdotool windowfocus $WID
xdotool windowactivate $WID


xdotool getactivewindow getwindowpid


xdotool key ctrl+r

xdotool key ctrl+a
xdotool key BackSpace

xdotool type $ps_automation_path
xdotool key Tab
xdotool key Tab

xdotool key ctrl+a
xdotool key BackSpace

xdotool key Tab
xdotool key Tab
xdotool key Return


