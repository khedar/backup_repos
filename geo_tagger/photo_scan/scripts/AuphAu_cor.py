#!/usr/bin/env python
# -*- coding: utf-8 -*-

## Created on 23.06.2016
## author: Simon T.

import PhotoScan as PS
import os
import glob
import configparser
import re

'''Automatisation skript called from PhotoScan'''

def processing(dwg, quality):
    '''Contains process-steps that are called in PhotoScan'''

    config = configparser.ConfigParser()
    cwd = os.path.dirname(os.path.abspath(__file__))
    config_file_path = cwd+'/config_cor.ini'
    config.read(config_file_path)

    input_dir_path = config["PATHS"]["input_images_path"]
    save_path = input_dir_path +"/PSResults/"+config["MISC"]["pro_name"]

    types = (input_dir_path+"/*.jpg",input_dir_path+"/*.JPG")
    all_fotos = []
    for files in types:
        all_fotos.extend(glob.glob(files))

    if not os.path.exists(os.path.dirname(save_path)):
         os.makedirs(os.path.dirname(save_path))

    doc = PS.app.document
    doc.save(path = save_path +".psx")

    # input_fotos = []
    #
    # for photo in all_fotos:
    #     print(photo)
    #     input_fotos.append(photo)

    chunk = doc.addChunk ()
    chunk.label = config["MISC"]["pro_name"] +"_"+ quality

    input_list_file = input_dir_path + '/list.txt'
    input_fotos = [line.rstrip('\n') for line in open(input_list_file)]
    print(input_fotos)
    chunk.addPhotos(input_fotos)

    if config["MISC"]["calibfile"] == "true":
        print("calib wird benutzt")
        calib = PS.Calibration()
        calib.load(config["PATHS"]["calibfile_path"])
    ###########################Nur eingeschraengt nutzbar!!####################
#    calib = PS.Calibration()
#    calib.load(calibfile_path)
    ###########################################################################


    doc.save(path = save_path +".psx")
    chunk.matchPhotos(dwg["accuracy"],dwg["preselection"], dwg["filter_mask"],
                      dwg["keypoint_limit"], dwg["tiepoint_limit"])

    chunk.alignCameras()
    chunk.optimizeCameras(fit_f=True, fit_cxcy=True, fit_b1=True, fit_b2=True,
                          fit_k1k2k3=True, fit_p1p2=True, fit_k4=True,
                          fit_p3=True, fit_p4=True)
    doc.save(path = save_path +".psx")

    ###########################################################################
    if dwg["no_DenseCloud"] == False:
        chunk.buildDenseCloud(dwg["quality"], dwg["filter"], keep_depth=False,
                              reuse_depth=False)
    else:
        pass
    ###########################################################################

    doc.save(path = save_path +".psx")

    chunk.buildModel(dwg["surface_M"], dwg["interpolation"], dwg["face_count"])

    doc.save(path = save_path +".psx")

    chunk.buildUV(PS.MappingMode.AdaptiveOrthophotoMapping,count=1)
    chunk.buildTexture(PS.BlendingMode.MosaicBlending, color_correction=False,
                       size=2048)

    doc.save(path = save_path +".psx")

    chunk.buildDem(dwg["source_cloud"], dwg["interpolation"])

    doc.save(path = save_path +".psx")

    chunk.buildOrthomosaic(dwg["surface_O"], dwg["blending"],
                           dwg["color_correction"])

    chunk.buildContours(dwg["source_data"], dwg["interval"])

    doc.save(path = save_path +".psx")

    return


def choose_profile(quality):
    '''creates a dict with options for the desired quality param in PhotoScan,
    depending on what quality-profile was choosen'''

    if quality == "low":
        dwg = {
               # for matchPhotos:
               "accuracy":      PS.Accuracy.LowAccuracy,
               "preselection":  PS.Preselection.GenericPreselection,
               "filter_mask":   True,
               "keypoint_limit": 20000,
               "tiepoint_limit": 4000,
               #for builddensecloud
               "quality":       PS.Quality.LowQuality,
               "filter" :       PS.FilterMode.AggressiveFiltering,
               # for buildModel
               "surface_M":     PS.SurfaceType.HeightField,
               "interpolation": PS.Interpolation.EnabledInterpolation,
               "face_count":    PS.FaceCount.LowFaceCount,
               # for DEM
               "source_cloud":  PS.DataSource.PointCloudData,
               #for Ortho
               "surface_O":     PS.DataSource.ElevationData,
               "blending":      PS.BlendingMode.MosaicBlending,
               "color_correction": False,

               #for Contour
               "source_data":   "elevation",
               "interval":      1,
               "no_DenseCloud": False

               }
        return dwg

    elif quality == "opt":

        dwg = {
               # for matchPhotos:
               "accuracy":      PS.Accuracy.LowestAccuracy,
               "preselection":  PS.Preselection.ReferencePreselection,
               "filter_mask":   True,
               "keypoint_limit": 20000,
               "tiepoint_limit": 4000,
               #for builddensecloud
               "quality":       PS.Quality.LowQuality,
               "filter" :       PS.FilterMode.AggressiveFiltering,
               # for buildModel
               "surface_M":     PS.SurfaceType.HeightField,
               "interpolation": PS.Interpolation.EnabledInterpolation,
               "face_count":    PS.FaceCount.LowFaceCount,
               # for DEM
               "source_cloud":  PS.DataSource.DenseCloudData,
               #for Ortho
               "surface_O":     PS.DataSource.ElevationData,
               "blending":      PS.BlendingMode.MosaicBlending,
               "color_correction": False,

               #for Contour
               "source_data":   "elevation",
               "interval":      1,
               "no_DenseCloud": False

               }
        return dwg
###############################################################################
#########################TEST ENVIROMENT#######################################
    elif quality == "test1":
        print("TEST ENVIROMENT")
        dwg = {
               # for matchPhotos:
               "accuracy":      PS.Accuracy.LowestAccuracy,
               "preselection":  PS.Preselection.ReferencePreselection,
               "filter_mask":   True,
               "keypoint_limit": 20000,
               "tiepoint_limit": 4000,
               #for builddensecloud
               "quality":       PS.Quality.LowQuality,
               "filter" :       PS.FilterMode.AggressiveFiltering,
               # for buildModel
               "surface_M":     PS.SurfaceType.HeightField,
               "interpolation": PS.Interpolation.EnabledInterpolation,
               "face_count":    PS.FaceCount.LowFaceCount,
               # for DEM
               "source_cloud":  PS.DataSource.DenseCloudData,
               #for Ortho
               "surface_O":     PS.DataSource.ElevationData,
               "blending":      PS.BlendingMode.MosaicBlending,
               "color_correction": False,

               #for Contour
               "source_data":   "elevation",
               "interval":      1,
               "no_DenseCloud": False

               }
        return dwg

    elif quality == "test2":
        print("TEST ENVIROMENT")
        dwg = {
               # for matchPhotos:
               "accuracy":      PS.Accuracy.LowestAccuracy,
               "preselection":  PS.Preselection.ReferencePreselection,
               "filter_mask":   True,
               "keypoint_limit": 20000,
               "tiepoint_limit": 4000,
               #for builddensecloud
               "quality":       PS.Quality.LowQuality,
               "filter" :       PS.FilterMode.AggressiveFiltering,
               # for buildModel
               "surface_M":     PS.SurfaceType.HeightField,
               "interpolation": PS.Interpolation.EnabledInterpolation,
               "face_count":    PS.FaceCount.LowFaceCount,
               # for DEM
               "source_cloud":  PS.DataSource.DenseCloudData,
               #for Ortho
               "surface_O":     PS.DataSource.ElevationData,
               "blending":      PS.BlendingMode.MosaicBlending,
               "color_correction": False,

               #for Contour
               "source_data":   "elevation",
               "interval":      1,
               "no_DenseCloud": False

               }
        return dwg

    elif quality == "test3":
        print("TEST ENVIROMENT")
        dwg = {
               # for matchPhotos:
               "accuracy":      PS.Accuracy.LowestAccuracy,
               "preselection":  PS.Preselection.ReferencePreselection,
               "filter_mask":   True,
               "keypoint_limit": 20000,
               "tiepoint_limit": 4000,
               #for builddensecloud
               "quality":       PS.Quality.LowQuality,
               "filter" :       PS.FilterMode.AggressiveFiltering,
               # for buildModel
               "surface_M":     PS.SurfaceType.HeightField,
               "interpolation": PS.Interpolation.EnabledInterpolation,
               "face_count":    PS.FaceCount.LowFaceCount,
               # for DEM
               "source_cloud":  PS.DataSource.DenseCloudData,
               #for Ortho
               "surface_O":     PS.DataSource.ElevationData,
               "blending":      PS.BlendingMode.MosaicBlending,
               "color_correction": False,

               #for Contour
               "source_data":   "elevation",
               "interval":      1,
               "no_DenseCloud": False

               }
        return dwg
###############################################################################
###############################################################################

    elif quality == "medium":

        dwg = {
               # for matchPhotos:
               "accuracy":      PS.Accuracy.MediumAccuracy,
               "preselection":  PS.Preselection.ReferencePreselection,
               "filter_mask":   True,
               "keypoint_limit": 40000,
               "tiepoint_limit": 4000,
               #for builddensecloud
               "quality":       PS.Quality.MediumQuality,
               "filter" :       PS.FilterMode.AggressiveFiltering,
               # for buildModel
               "surface_M":     PS.SurfaceType.HeightField,
               "interpolation": PS.Interpolation.EnabledInterpolation,
               "face_count":    PS.FaceCount.LowFaceCount,
               # for DEM
               "source_cloud":  PS.DataSource.DenseCloudData,
               #for Ortho
               "surface_O":     PS.DataSource.ElevationData,
               "blending":      PS.BlendingMode.MosaicBlending,
               "color_correction": False,

               #for Contour
               "source_data":   "elevation",
               "interval":      1,
               "no_DenseCloud": False
               }
        return dwg

    elif quality == "high":

        dwg = {
               # for matchPhotos:
               "accuracy":      PS.Accuracy.HighAccuracy,
               "preselection":  PS.Preselection.ReferencePreselection,
               "filter_mask":   True,
               "keypoint_limit": 0,
               "tiepoint_limit": 4000,
               #for builddensecloud
               "quality":       PS.Quality.UltraQuality,
               "filter" :       PS.FilterMode.AggressiveFiltering,
               # for buildModel
               "surface_M":     PS.SurfaceType.HeightField,
               "interpolation": PS.Interpolation.EnabledInterpolation,
               "face_count":    PS.FaceCount.HighFaceCount,
               # for DEM
               "source_cloud":  PS.DataSource.DenseCloudData,
               #for Ortho
               "surface_O":     PS.DataSource.ElevationData,
               "blending":      PS.BlendingMode.AverageBlending,
               "color_correction": False,

               #for Contour
               "source_data":   "elevation",
               "interval":      1,
               "no_DenseCloud": False
               }
        return dwg


    elif quality == "lowest":

        dwg = {
               # for matchPhotos:
               "accuracy":      PS.Accuracy.LowestAccuracy,
               "preselection":  PS.Preselection.GenericPreselection,
               "filter_mask":   True,
               "keypoint_limit": 20000,
               "tiepoint_limit": 4000,
               #for builddensecloud
               "quality":       PS.Quality.LowestQuality,
               "filter" :       PS.FilterMode.AggressiveFiltering,
               # for buildModel
               "surface_M":     PS.SurfaceType.HeightField,
               "interpolation": PS.Interpolation.EnabledInterpolation,
               "face_count":    PS.FaceCount.LowFaceCount,
               # for DEM
               "source_cloud":  PS.DataSource.PointCloudData,
               #for Ortho
               "surface_O":     PS.DataSource.ElevationData,
               "blending":      PS.BlendingMode.AverageBlending,
               "color_correction": False,

               #for Contour
               "source_data":   "elevation",
               "interval":      1,
               "no_DenseCloud": False
               }
        return dwg

    else:
        print ("noooo_TEST")
        return

def export(qual):
    '''exports the results from the project, into desired formats'''
    print ("#############################")
    print ("########Exporting############")
    print ("#############################")

    doc = PS.app.document

    config = configparser.ConfigParser()
    cwd = os.path.dirname(os.path.abspath(__file__))
    config_file_path = cwd+'/config_cor.ini'
    config.read(config_file_path)
    save_path = config["PATHS"]["input_images_path"] +"/PSResults/"
    crs = PhotoScan.CoordinateSystem("EPSG::32632")
    for chunk in doc.chunks:
        if qual == chunk.label.split("_")[-1]:
            print(chunk.label.split("_")[-1])

            orhtopfadjpg = save_path +"ortho_"+qual +".jpg"
            chunk.exportOrthomosaic(path = orhtopfadjpg, format = "jpg",
                            raster_transform = PS.RasterTransformType.RasterTransformNone,
                            write_kml = False, write_world = False,
                            tiff_compression = "none", tiff_big = False)

            orhtopfadtiff = save_path +"ortho_"+qual +".tif"
            chunk.exportOrthomosaic(path = orhtopfadtiff, format = "tif",
                            raster_transform = PS.RasterTransformType.RasterTransformNone,
                            write_kml = False, write_world = False,
                            tiff_compression = "none", tiff_big = False, projection=crs)

            dempfad = save_path +"dem_"+qual +".tif"
            chunk.exportDem(path = dempfad, format = "tif", nodata =-32767,
                        write_kml = False, write_world = False, tiff_big=False)

            modelpfad = save_path +"model_"+qual +".ply"
            chunk.exportModel(path = modelpfad, precision=6,texture=True,
                          normals=True, colors=False, cameras = True,
                          udim = False,strip_extensions=False, projection=crs)

            pointcloudfad = save_path +"ptcloud_"+qual +".ply"
            chunk.exportPoints(path = pointcloudfad, binary=True, precision=6,
            	          normals=True,colors=True, projection=crs)

            pointcloudasciifad = save_path +"ptcloudascii_"+qual +".ply"
            chunk.exportPoints(path = pointcloudasciifad, binary=False, precision=6,
                        normals=True,colors=True, projection=crs)

            reppfad = save_path +"rep_"+qual +".pdf"

            chunk.exportReport(reppfad,chunk.label)



def main():
    '''reads out the desired quality-profiles from config.ini. For each Profile
    the a automatisiation-run is called'''

    config = configparser.ConfigParser()

    cwd = os.path.dirname(os.path.abspath(__file__))
    config_file_path = cwd+'/config_cor.ini'
    config.read(config_file_path)
    qual_ls = config.get("QUALITY", "qual")
    qual_list = qual_ls.replace("'","").replace(" ","").split(",")

    for qual in qual_list:
        dict_with_quality = choose_profile(qual)
        processing(dict_with_quality, qual)
        export(qual)
    PS.app.quit()



main()
