
#ifndef GEO_TAGGER_H
#define GEO_TAGGER_H

#include <ros/ros.h>
#include <opencv2/highgui/highgui.hpp>

#include <cv_bridge/cv_bridge.h>
#include <boost/filesystem/operations.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "sensor_msgs/NavSatFix.h"
#include "custom_msgs/GeoImageCompressed.h"
#include "custom_msgs/PhotoScan.h"

#include "mission_control/Mission.h"

#include "std_srvs/Trigger.h"

#include <exiv2/exiv2.hpp>

#include <fstream>
#include <cstdlib>

#include <sys/types.h>
#include <pwd.h>

#include <string>

#include "geo_tagger_helper.h"

class GeoTagger
{
  public:
    GeoTagger();
  private:
    void gpsCb(const sensor_msgs::NavSatFix::ConstPtr& mGPSmsg)
    {
      mGeoData.mGPS = *mGPSmsg;
      ROS_INFO_STREAM_THROTTLE(10,"Throttled Info.GPS Values in gpsCb()  "<<"Long: "<< mGeoData.mGPS.longitude<< \
      				  " Lat: "<<mGeoData.mGPS.latitude<<" Alt: "<<mGeoData.mGPS.altitude);
    }
    void missionStateCb(const mission_control::Mission::ConstPtr& mission);
    void imageCb(const sensor_msgs::CompressedImageConstPtr& message);
    bool startGeoSave(std_srvs::Trigger::Request& req,std_srvs::Trigger::Response& res);
    bool stopGeoSave(std_srvs::Trigger::Request& req, std_srvs::Trigger::Response& res);
    void arHeadingCb(const std_msgs::Float32ConstPtr& heading_message)
    {
          mGeoData.mCurrentHeading = heading_message->data;
    }
    void arBaroHeightCb(const std_msgs::Float32ConstPtr& baroheight_message)
    {
          mGeoData.mCurrentBaroHeight = baroheight_message->data;
    }
    void currentImageName()
    {
      mImgName = mImgSaveLocation + "/" + mCamName + std::to_string(mImgCount) + ".jpg";
      mImgCount++;
    }
    std::string mCamName, mImgName, mImgSaveLocation;

    ExifTagData mExifTagData;
    ImageInfo mImgInfo;

    uint32_t mImgCount, mMissionCount;
    bool mMissionStateOn_;
    bool mSaveStartSrvOn_;
    GeoTagData mGeoData;
    custom_msgs::GeoImageCompressed mGeoImgROSMsg;
    ros::NodeHandle mNH,mPrivateNH;
    ros::Publisher mGeoImgPublisher;
    ros::Subscriber mImgSubscriber;
    ros::Subscriber mCurrentHeadingSubscriber;
    ros::Subscriber mBaroHeightSubscriber;
    ros::Subscriber mGPSSubscriber;
    ros::Subscriber mMissionStateSubscriber;
    ros::ServiceServer mImgSaveStartService;
    ros::ServiceServer mImgSaveStopService;
};

#endif // GEO_TAGGER_H
