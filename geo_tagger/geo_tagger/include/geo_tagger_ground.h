
#ifndef GEO_TAGGER_GROUND_H
#define GEO_TAGGER_GROUND_H

#include <ros/ros.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <boost/filesystem/operations.hpp>
#include "boost/date_time/posix_time/posix_time.hpp"

#include "custom_msgs/GeoImageCompressed.h"
#include "custom_msgs/PhotoScan.h"

#include "std_srvs/Trigger.h"

#include <exiv2/exiv2.hpp>

#include <fstream>
#include <cstdlib>

#include <sys/types.h>
#include <pwd.h>

#include <string>

#include "geo_tagger_helper.h"

class GeoTaggerGround
{
  public:
    GeoTaggerGround();
    void PublishPSMsg();
  private:
    void geoImgCb(const custom_msgs::GeoImageCompressedConstPtr& message);

    std::string mCamName, mImgSaveLocation;
    ExifTagData mExifTagData;
    uint32_t mMissionCount;
    bool mPhotoScanStart_;

    ros::NodeHandle mNH,mPrivateNH;
    ros::Publisher mPSInfoPublisher;
    ros::Subscriber mGeoImgSubscriber;
    ros::ServiceClient mPSServiceClient;
};

#endif // GEO_TAGGER_GROUND_H
