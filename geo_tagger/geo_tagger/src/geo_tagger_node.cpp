#include "../include/geo_tagger.h"

int main(int argc, char **argv)
{
  ros::init(argc, argv, "geo_tagger_flight_node");
  GeoTagger geotagger_;
  ros::Rate rate(10);
  while (ros::ok()) // Loop until shutdown
  {
    ros::spinOnce(); // handle callbacks
    rate.sleep();
  }
  return 0;
}

GeoTagger::GeoTagger(): mImgCount(0), mMissionCount(1000), mMissionStateOn_(false),
                        mSaveStartSrvOn_(false), mPrivateNH("~")
{
  mCurrentHeadingSubscriber    = mNH.subscribe("/ar_heading", 200,&GeoTagger::arHeadingCb, this);
  mBaroHeightSubscriber        = mNH.subscribe("/ar_baro_height", 200,&GeoTagger::arBaroHeightCb, this);
  mGPSSubscriber               = mNH.subscribe("/tripos/singlepos/fix",200,&GeoTagger::gpsCb, this);
  mMissionStateSubscriber      = mNH.subscribe("/my_state",2,&GeoTagger::missionStateCb, this); // Topic only on copter
  //- Get data from parametr server
  std::string flight_save_start_srv, flight_save_stop_srv, mImgTopic, mGeoImgTopic;
  mPrivateNH.param("image_topic", mImgTopic, std::string("uninitialised"));
  mPrivateNH.param("camera_name", mCamName, std::string("uninitialised"));
  mPrivateNH.param("geoimage_topic", mGeoImgTopic, std::string("uninitialised"));
  mPrivateNH.param("flight_save_start_srv", flight_save_start_srv, std::string("uninitialised"));
  mPrivateNH.param("flight_save_stop_srv", flight_save_stop_srv, std::string("uninitialised"));

  mImgSaveStartService = mNH.advertiseService(flight_save_start_srv, &GeoTagger::startGeoSave, this);
  mImgSaveStopService = mNH.advertiseService(flight_save_stop_srv, &GeoTagger::stopGeoSave, this);

  mImgSubscriber = mNH.subscribe(mImgTopic, 200,&GeoTagger::imageCb, this);
  mGeoImgPublisher = mNH.advertise<custom_msgs::GeoImageCompressed>(mGeoImgTopic,200);
  ROS_INFO_STREAM("\n" << "GeoTagger has been successfully created."  << "\n" \
  				  "Image Topic Subscribed is: 	   " << mImgTopic    	<< "\n" \
  				  "Camera Name is:				   " << mCamName 	    << "\n" \
  				  "GeoImage is Published on topic: " << mGeoImgTopic);
  // TODO Initialise the Messages to null values

}

void GeoTagger::imageCb(const sensor_msgs::CompressedImageConstPtr& message)
{
  if (mGeoData.mGPS.status.status != 1)
  {
  	ROS_WARN_STREAM_THROTTLE(10,"No GPS Fix. No GeoImage will be saved or published.");
  	return;
  }
  if (mSaveStartSrvOn_||mMissionStateOn_)
  {
    currentImageName();
    // Publish GeoImage
    ros::Time lCurrentTime = ros::Time::now();
    mImgInfo.mCamName = mCamName;
    mImgInfo.mImgName = mImgName;
    mImgInfo.mImgCount = mImgCount;

    boost::posix_time::ptime my_posix_time = lCurrentTime.toBoost();
    mImgInfo.mImgOrigTime = boost::posix_time::to_simple_string(my_posix_time);
    // Note to Me: Should Use GPS time? Not so big diff. Don't think about it again.

    mGeoImgROSMsg.imagename.data=mImgName.substr(mImgSaveLocation.length());
    mGeoImgROSMsg.imagedata = *message;
    mGeoImgROSMsg.gpsdata=mGeoData.mGPS;
    mGeoImgROSMsg.heading.data = mGeoData.mCurrentHeading;
    mGeoImgROSMsg.baroHeight.data = mGeoData.mCurrentBaroHeight;
    mGeoImgROSMsg.header.stamp = lCurrentTime;
    mGeoImgROSMsg.header.seq  = mImgCount;
    mGeoImgROSMsg.missioncount.data = mMissionCount;
    mGeoImgPublisher.publish(mGeoImgROSMsg);

    // Save Geotagged Image as JPG
    ExivImagePtr limage = Exiv2::ImageFactory::open(&(message->data)[0], message->data.size());
    geoTagImage(limage,mImgInfo, mGeoData);
  }
}

void GeoTagger::missionStateCb(const mission_control::Mission::ConstPtr& mission)
{
  if ( (mission->enabled && (mission->missiontype == mission_control::Mission::PATH) )  ||  (mission->payload_enabled) )
  {
    ROS_INFO_STREAM_THROTTLE(10,"Mission is on.");
    if ( mMissionCount != mission->msg_id )
    {
      mImgSaveLocation = getFlightSaveLocation(mCamName);
      createSaveLocation(mImgSaveLocation);
      mImgInfo.mProjectName = mImgSaveLocation.substr(mImgSaveLocation.size()-13);
      mImgCount = 0;
      ROS_INFO_STREAM("New Mission started. MissionId is: " << mission->msg_id);
      mMissionCount = mission->msg_id;
      mMissionStateOn_ = true;
      mSaveStartSrvOn_ = false;
    }
  }
  else if ( (mission->enabled == false) && (mission->payload_enabled == false) && (mMissionStateOn_) )
  {
    ROS_WARN_STREAM_THROTTLE(10,"Mission is off.");
    mMissionStateOn_ = false;
    mMissionCount = 12321;  // Special Mission ID, means stopped mission.
    mGeoImgROSMsg.missioncount.data = mMissionCount;
    mGeoImgPublisher.publish(mGeoImgROSMsg); // Publish One last time for the sake of groundnode
  }
}

bool GeoTagger::startGeoSave(std_srvs::Trigger::Request& req, std_srvs::Trigger::Response& res)
{
  if (mMissionStateOn_)
  {
    ROS_WARN_STREAM("Requested (startGeoSave) but failed because Mission is on.");
    return false;
  }
  mMissionCount = mMissionCount + 100;
  mSaveStartSrvOn_ = true;
  res.success = mSaveStartSrvOn_;
  ROS_INFO_STREAM("Requested (startGeoSave) and succeded. MissionId is: " << mMissionCount);
  mImgSaveLocation = getFlightSaveLocation(mCamName);
  createSaveLocation(mImgSaveLocation);
  mImgInfo.mProjectName = mImgSaveLocation.substr(mImgSaveLocation.size()-13);
  mImgCount = 0;
  return true;
}

bool GeoTagger::stopGeoSave(std_srvs::Trigger::Request& req, std_srvs::Trigger::Response& res)
{
  mSaveStartSrvOn_ = false;
  res.success = mSaveStartSrvOn_;
  ROS_INFO_STREAM("Requested (stopGeoSave) and succeded.");
  return true;
}
