#include "../include/geo_tagger_ground.h"

int main(int argc, char **argv)
{
  ros::init(argc, argv, "geo_tagger_ground_node");
  GeoTaggerGround geotagger_;
  //ros::spin();
  ros::Rate rate(100);
  while (ros::ok()) // Loop until shutdown
  {
    ros::spinOnce(); // handle callbacks
    geotagger_.PublishPSMsg();
    rate.sleep();
  }
  return 0;
}
GeoTaggerGround::GeoTaggerGround(): mMissionCount(1000), mPhotoScanStart_(false), mPrivateNH("~")
{
  std::string lPSStartSrv,lGeoImgTopic,lPSInfoTopic;
  mPrivateNH.param("geoimage_topic", lGeoImgTopic, std::string("uninitialised"));
  mPrivateNH.param("camera_name", mCamName, std::string("uninitialised"));
  mPrivateNH.param("photo_scan_start_sub_topic", lPSInfoTopic, std::string("uninitialised"));
  mPrivateNH.param("photo_scan_start_srv", lPSStartSrv, std::string("uninitialised"));


  mGeoImgSubscriber = mNH.subscribe(lGeoImgTopic, 1000, &GeoTaggerGround::geoImgCb, this);
  mPSInfoPublisher   = mNH.advertise<custom_msgs::PhotoScan>(lPSInfoTopic,200);
  mPSServiceClient = mNH.serviceClient<std_srvs::Trigger>(lPSStartSrv);

  ROS_INFO_STREAM("\n" << "GeoTaggerGround Ground has been successfully created." << "\n" \
  				  "PS Start Msg Topic Subscribed is:" << lPSInfoTopic    	<< "\n" \
  				  "Camera Name is:				          " << mCamName 	      << "\n" \
            "PS Start Service:			          " << lPSStartSrv      << "\n" \
  				  "GeoImage is Subscribed on topic: " << lGeoImgTopic);
}

void GeoTaggerGround::geoImgCb(const custom_msgs::GeoImageCompressedConstPtr& message)
{
  ROS_INFO_STREAM("Check New Mission. Current Count is: " << mMissionCount << "New Count is: " << message->missioncount.data);
  if ( ( message->missioncount.data != mMissionCount ) )
  {
    // Start PS
    ROS_INFO_STREAM("Starting New Mission.");
    if (message->missioncount.data == 12321) {
      ROS_INFO_STREAM("Mission Ended. Calling PS to start.");
      std_srvs::Trigger srv;
      mPSServiceClient.call(srv);
      return;
    }
    mImgSaveLocation = getFlightSaveLocation(mCamName);
    createSaveLocation(mImgSaveLocation);
    ROS_INFO_STREAM("New Mission started.");
    mMissionCount = message->missioncount.data;
  }

  GeoTagData lGeoData;
  lGeoData.mGPS = message->gpsdata;
  lGeoData.mCurrentHeading = message->heading.data;
  lGeoData.mCurrentBaroHeight = message->baroHeight.data;
  std::string lImgName = mImgSaveLocation + message->imagename.data;
  ImageInfo lImgInfo;
  lImgInfo.mCamName = mCamName;
  lImgInfo.mImgName = lImgName;
  lImgInfo.mImgCount = message->header.seq;
  lImgInfo.mProjectName = mImgSaveLocation.substr(mImgSaveLocation.size()-13);

  boost::posix_time::ptime my_posix_time = message->header.stamp.toBoost();
  lImgInfo.mImgOrigTime = boost::posix_time::to_simple_string(my_posix_time);

  // Save Geotagged Image as JPG
  ExivImagePtr image = Exiv2::ImageFactory::open(&(message->imagedata.data)[0], message->imagedata.data.size());
  geoTagImage(image,lImgInfo, lGeoData);

}
void GeoTaggerGround::PublishPSMsg()
{
  ROS_INFO_STREAM_THROTTLE(5,"PS Save Loction:" << mImgSaveLocation);
  custom_msgs::PhotoScan lPhotoScanMsg;
  lPhotoScanMsg.savelocation.data = mImgSaveLocation;
  lPhotoScanMsg.startphotoscan.data = mPhotoScanStart_;
  mPSInfoPublisher.publish(lPhotoScanMsg);
}
