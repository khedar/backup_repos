
#include <ros/ros.h>
#include <ros/package.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <exiv2/exiv2.hpp>
#include "sensor_msgs/NavSatFix.h"
#include "custom_msgs/GeoImageCompressed.h"
#include "custom_msgs/PhotoScan.h"
#include "mission_control/Mission.h"

#include <chrono>

typedef std::unique_ptr< Exiv2::Image > ExivImagePtr;
using std::chrono::system_clock;

std::string image_topic("/manta/camera/image_color");
std::string gps_topic("/tripos/singlepos/fix");
std::string heading_topic("/ar_heading");
std::string rel_alt_topic("/ar_baro_height");
std::string mission_state_topic("/my_state");

class TestGeoTagger
{
  public:
    TestGeoTagger(): mCount(0), mPrivateNH("~"),mIT(mNH)
    {
      mPrivateNH.param("image_topic", image_topic, std::string("/manta/camera/image_color"));
      mPrivateNH.param("gps_topic", gps_topic, std::string("/tripos/singlepos/fix"));
      mPrivateNH.param("heading_topic", heading_topic, std::string("/ar_heading"));
      mPrivateNH.param("rel_alt_topic", rel_alt_topic, std::string("/ar_baro_height"));
      mPrivateNH.param("mission_state_topic", mission_state_topic, std::string("/my_state"));

      mImgPub = mIT.advertise(image_topic,1);
      mGPSPub = mNH.advertise<sensor_msgs::NavSatFix>(gps_topic,1);
      mHeadingPub = mNH.advertise<std_msgs::Float32>(heading_topic,1);
      mRelAltPub = mNH.advertise<std_msgs::Float32>(rel_alt_topic,1);
      mMissionStatePub = mNH.advertise<mission_control::Mission>(mission_state_topic,1);

      cv::Mat image = cv::imread(ros::package::getPath("geo_tagger")+"/test/test.JPG", CV_LOAD_IMAGE_COLOR);
      mImg = *cv_bridge::CvImage(std_msgs::Header(), "bgr8", image).toImageMsg();

      // Fixed Data which wont change during the test.
      mGPS.longitude = 5.123456;
      mGPS.latitude = 10.123456;
      mGPS.altitude = 110.123;

      mHeading.data = 150.123;
      mRelAlt.data = 80.123;

    }
    void Publish()
    {
      mImgPub.publish(mImg);
      mGPSPub.publish(mGPS);
      mHeadingPub.publish(mHeading);
      mRelAltPub.publish(mRelAlt);
      mMissionStatePub.publish(mMissionState);
    }

  private:
    uint32_t mCount;
    sensor_msgs::Image mImg;
    sensor_msgs::NavSatFix mGPS;
    std_msgs::Float32 mHeading;
    std_msgs::Float32 mRelAlt;
    mission_control::Mission mMissionState;

    ros::NodeHandle mNH,mPrivateNH;
    image_transport::ImageTransport mIT;
    image_transport::Publisher mImgPub;
    ros::Publisher mGPSPub;
    ros::Publisher mHeadingPub;
    ros::Publisher mRelAltPub;
    ros::Publisher mMissionStatePub;

};

int main(int argc, char **argv)
{
  ros::init(argc, argv, "test_geo_tagger_ground_node");
  TestGeoTagger aTester;
  ros::Rate rate(10);
  while (ros::ok()) // Loop until shutdown
  {
    ros::spinOnce(); // handle callbacks
    aTester.Publish();
    rate.sleep();
  }
  return 0;
}
