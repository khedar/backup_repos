custom_msgs
=========

This package contains custom message definitions for iFF project Ankommen.

1. GeoImage - Combination of NavSatFix and Image messages
1. GeoImageCompressed - Combination of NavSatFix and CompressedImage messages

