# custom_msgs
Contains custom ros messages created for ANKOMMEN project at IFF

DepthStampedId.msg
GeoImage.msg
GeoImageCompressed.msg
PhotoScan.msg
PoseStampedId.msg
StitchedImageCompressed.msg

Read the README file of the custom_msgs package for more
details.

# geo_tagger
Geo Tagger package for geo tagging images with Exif tags.
It has two nodes, one for flight and another for ground. 
Works with Color camera from AVT and Thermal from FLIR.

Read the README file of the geo_tagger package for more
details.

# photo_scan
Node for auto starting of Photo scan software and then
publishing the results.

Read the README file of the photo_scan package for more
details.
