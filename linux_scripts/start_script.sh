#! /bin/bash

cd /home/odroid

export ROS_ROOT=/opt/ros/kinetic/share/ros
export ROS_DISTRO=kinetic
export ROS_PACKAGE_PATH=/opt/ros/kinetic/share
export PATH=$PATH:/opt/ros/kinetic/bin
export LD_LIBRARY_PATH=/opt/ros/kinetic/lib
export PYTHONPATH=/opt/ros/kinetic/lib/python2.7/dist-packages
export ROS_MASTER_URI=http://localhost:11311
export CMAKE_PREFIX_PATH=/opt/ros/kinetic

source /home/odroid/Documents/workspace/catkin_ws/imav2017/devel/setup.bash

#roslaunch launch imav17.launch </dev/null &>/dev/null &
roslaunch launch imav17.launch

sleep 10

rosrun mavros mavsys rate --all 10

#sleep 20 

#rosservice call /start_save_flight

#rosbag record /mavros/global_position /camera/image_raw/compressed/geotagged /camera/image_raw

#roslaunch SARMAP
